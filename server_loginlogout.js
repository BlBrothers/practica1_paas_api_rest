
var express=require('express'); //inclusión de express
var app=express();
// Add the bodyparser component to interpretate the body part
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var port=process.env.PORT || 3000; //Puerto para express

app.listen(port); //ponemos a escuchar
console.log("API escuchando en el puerto " + port);

//implementation of the read all users method
app.get('/apitechu/v1/users',
   function(req,res) {
       console.log("GET /apitechu/v1/users");
       res.sendFile('usuarios_emails.json', {root:__dirname});
   }
);

//implementation of login
//IN: req.body.email , req.body.password
/*OUT: {
 "mensaje" : "Login correcto",
 "idUsuario" : 1
}
*/
app.post('/apitechu/v1/login',
   function(req,res) {
       console.log("POST /apitechu/v1/login");

       var userToLogin = {
         "email" : req.body.email,
         "password" : req.body.password
       };

       console.log("Email " + req.body.email);
       console.log("Pwd " + req.body.password);

       var users = require('./usuarios_emails.json');

       for (user of users) {

         if ((user.email == req.body.email) && (user.password == req.body.password)) {

             console.log("Id " + user.id);
             user.logged = true;
             WriteUserDataToFile (users);
             var userFound = true;
             res.send({
                 "msg":"login correcto",
                 "IdUsuario":user.id
                 }
               );
             break;
           }
       }
       if (!userFound) {
         res.send({
             "msg":"login incorrecto"
             }
           );
       }
   }
);

//implementation of logout
//IN: req.body.id
/*OUT:{
 "mensaje" : "logout correcto"
 "idUsuario" : req.body.id
}
*/
app.post('/apitechu/v1/logout',
   function(req,res) {
       console.log("POST /apitechu/v1/logout");

       var users = require('./usuarios_emails.json');

       for (user of users) {

         if ((user.id == req.body.id) && (user.logged === true)) {

             console.log("Id found " + user.id);
             delete user.logged;
             WriteUserDataToFile (users);
             var userFound = true;
             res.send({
                 "msg":"logout correcto",
                 "IdUsuario":user.id
                 }
               );
             break;
           }
       }
       if (!userFound) {
         res.send({
             "msg":"logout incorrecto"
             }
           );
       }
   }
);


// Function to save data to a filesystem
// IN: json data
function WriteUserDataToFile(data) {
  //add the component to manage filesystem
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    "./usuarios_emails.json",
    jsonUserData,
    "utf8",
    function(err) {
      if (err) {
        console.log("err");
      } else {
        console.log("Users file persisted!");
      }
    }
  );
}
