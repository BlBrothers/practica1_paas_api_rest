
var express=require('express'); //inclusión de express
var app=express();
// Add the bodyparser component to interpretate the body part
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var jwt=require('jsonwebtoken');
var expressJwt=require('express-jwt');
var jwtClave="apitechuv2login";
app.use(expressJwt({secret:jwtClave}).unless({path: ["/apitechu/v2/login","/apitechu/v2/register" ]}));

var port=process.env.PORT || 3001; //Puerto para express
app.listen(port); //ponemos a escuchar
console.log("API escuchando en el puerto " + port);

var requestJson = require('request-json'); // todo lo que se va a
var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumvl/collections/";
var mLabAPIKey = "apiKey=aVWUGiSNmf5rRIog9v9aN_I6Rp2lsX01";

const uuidV1 = require('uuid/v1');

//implementation of the read all users method from mongodb
app.get('/apitechu/v2/users',
   function(req,res) {
       console.log("GET /apitechu/v2/users");

       httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP creado");

       httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error al recuperar usuarios"
          }
          res.send(response);
        }
     );
   }
);


//implementation of the read one user method from mongodb searching by id
app.get('/apitechu/v2/users/:id',
   function(req,res) {
       console.log("GET /apitechu/v2/users/:id");

       var id = req.params.id;
       var query = 'q={"id" : ' + id + '}';

       httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP creado");

       httpClient.get("user?" + query + "&" + mLabAPIKey,
        // this is the first version of the err handler
        /*function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error al recuperar usuarios"
          }
          res.send(response);
        }*/
        function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo usuario."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body[0];
             } else {
               response = {
                 "msg" : "Usuario no encontrado."
               };
               res.status(404);
             }
           }
           res.send(response);
         }
     );
   }
);

//Login method
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;

   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Unable to login: email y/o passsword not found. :-("
         }
         res.status(404);
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var token = jwt.sign({
                  id:body[0].id
                  },jwtClave);
             var response = {
               "msg" : "Login succesfully! :-)",
               "nameUsuario" : body[0].first_name + " " + body[0].last_name,
               "idUsuario" : body[0].id,
               "token": token
             }
             res.status(200);
             res.send(response);
           }
         )
       }
     }
   );
 }
)

//Logout method
//Id is a query parameter
app.post('/apitechu/v2/logout',
 function(req, res) {
   console.log("POST /apitechu/v2/logout");
   var idUser = req.body.id;
   var query = 'q={"id": ' + idUser + '}';
   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + idUser +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : idUser
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
)

//implementation of the read accounts using a given userId method
app.get('/apitechu/v2/users/:id/accounts',
   function(req,res) {
       console.log("GET /apitechu/v2/users/:id/accounts");

       var id = req.params.id;
       var query = 'q={"userid" : ' + id + '}';

       httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP creado");

       httpClient.get("account?" + query + "&" + mLabAPIKey,
        // this is the first version of the err handler
        /*function(err, resMLab, body){
          var response = !err ? body : {
            "msg" : "Error al recuperar usuarios"
          }
          res.send(response);
        }*/
        function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo cuentas del usuario."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body;
             } else {
               response = {
                 "msg" : "Usuario no encontrado."
               };
               res.status(404);
             }
           }
           res.send(response);
         }
     );
   }
);

//Logout method
//Email is a query parameter
app.post('/apitechu/v2/register',
 function(req, res) {
   console.log("POST /apitechu/v2/register");
   var nameUser = req.body.first_name;
   var surnameUser = req.body.last_name;
   var emailUser = req.body.email;
   //Search if the email is already in the db
   var querysearch = 'q={"email":"' + emailUser + '"}'; //q={"email":%20"rseager0@google.fr"}
   console.log("query de búsqueda es: " + querysearch);

   if ((nameUser!=undefined)&(surnameUser!=undefined)&(emailUser!=undefined)&(emailUser!="")){
     httpClient = requestJson.createClient(baseMLabURL);
     httpClient.get("user?" + querysearch + "&" + mLabAPIKey,
       function(err, resMLab, body) {
         console.log(body);
         if (body.length == 0) {
           console.log("The given user is not found in db... :-");

           //var id = generate a new Id value adding one to the last value of the db
           var queryget = 's={"id":-1}&l=1' ;
           console.log("query de getNewId construída es: " + queryget);
           //https://api.mlab.com/api/1/databases/apitechumvl/collections/user?s={%22id%22:-1}&l=1&apiKey=aVWUGiSNmf5rRIog9v9aN_I6Rp2lsX01

           httpClient = requestJson.createClient(baseMLabURL);
           httpClient.get("user?" + queryget + "&" + mLabAPIKey,
             function(err, resMLab, bodyGet) {
               if (bodyGet.length == 0) {
                 console.log("There is an error... :-(");
                 var response = {
                   "msg" : "There is an error... :-("
                 }
                 res.status(500);
                 res.send(response);
               }
               else {
                 console.log("Got new userID!!!");
                 var newId = bodyGet[0].id + 1;
                 query = '{"id":' + newId + ',"first_name":"' + nameUser + '","last_name":"' + surnameUser + '","email":"' + emailUser +'","password":"P@ssword"' + '}';
                 console.log("Query for put is " + query);
                 httpClient.post("user?" + mLabAPIKey, JSON.parse(query),
                  function(errPOST, resMLabPOST, bodyPOST) {
                     console.log("New user registered ... :-)");
                     var response = {
                       "msg" : "User registered succesfully",
                       "idNewUser": newId
                     }
                     res.status(200);
                     res.send(response);
                   }
                 );
               }
             }
           );
         }
         else {
           var response = {
             "msg" : "User already in the system. Please login... ;-)"
           }
           res.status(200);
           res.send(response);
         };
       }
     );
   }
   else{
     //The data in the body are incomplete
     response = {
       "msg" : "There is not all the necesary data... :-("
     }
     res.status(200);
     res.send(response);
   }
 }
)

//implementation of the read movements using a given account method
app.get('/apitechu/v2/accounts/:id/movements',
   function(req,res) {
       console.log("GET /apitechu/v2/accounts/:id/movements");

       var id = req.params.id;
       var query = 'q={"idaccount":' + id + '}';

       httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP nuevo creado con query:" + query);

       httpClient.get("movement?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error getting movements of an IBAN... :-("
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body;
             } else {
               response = {
                 "msg" : "IBAN not found."
               };
               res.status(404);
             }
           }
           res.send(response);
         }
     );
   }
);

//implementation of the transfer order

app.post('/apitechu/v2/transfer',
 function(req, res) {
   console.log("POST /apitechu/v2/transfer");

   var idAccountFrom = req.body.account_from;
   var balFrom = parseInt(req.body.balance_from);
   console.log ("balance cuenta" + balFrom);
   var ibanAccountTo = req.body.account_to;
   var nameAccountTo = req.body.name_to;
   var concept = req.body.concept;
   var amountToTransfer = parseInt(req.body.amount);
   var f = new Date();
   var orderDate = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear()
   var response = "No change";

   // call to update the balance of the accountfrom. DATA: accountId, amount to add.
   //var newBalanceFrom = ((operationToDo == "+") ? balFrom + amountToTransfer : balFrom - amountToTransfer);
   newBalanceFrom = balFrom - amountToTransfer;
   console.log("newBalanceFrom = "+ newBalanceFrom)
   var querysearch = 'q={"id":' + idAccountFrom + '}'; //q={"id":1}
   var putBody = '{"$set":{"balance":' + newBalanceFrom +'}}';
   console.log ("putBody " + putBody);
   var newId = uuidV1();

   httpClient = requestJson.createClient(baseMLabURL);

   httpClient.put("account?" + querysearch + "&" + mLabAPIKey, JSON.parse(putBody),
     function(errPUT, resMLabPUT, bodyPUT) {
       if (!errPUT) {
         console.log("Account updated");

         var query = '{"id":"' + newId + '","IBAN":"' + ibanAccountTo + '","amount":"-' + amountToTransfer + '","date":"' + orderDate + '","concept":"' + concept + '","idaccount":' + idAccountFrom + '}';
         console.log("Query to create new movement is " + query);

         httpClient.post("movement?" + mLabAPIKey, JSON.parse(query),
          function(errPOST, resMLabPOST, bodyPOST) {
            if (!errPOST){
              console.log("New movement created ... :-)");
              var response = {
                "msg" : "New movement created... :-)"
              };
              res.status = 200;
            }
            else{
              console.log("Rolling back the transfer ... :-)");
              var querysearchrollback = 'q={"id":' + idAccountFrom + '}'; //q={"id":1}
              var putBodyrollBack = '{"$set":{"balance":' + balFrom +'}}';
              httpClient.put("account?" + querysearchrollback + "&" + mLabAPIKey, JSON.parse(putBodyrollBack),
                function(errPUT, resMLabPUT, bodyPUT) {
                  if (!errPUT) {
                    console.log("Unable to transfer the money ... :-)");
                    var response = {
                      "msg" : "There was an error making the transfer... :-("
                    }
                  }
                }
              );
              var response = {
                "msg" : "There was an error making the transfer... :-("
              }
            }
            res.send(response);
          }
         );
       }
       else{
         var response = {
           "msg" : "There was an error making the transfer... :-("
         };
         res.send(response);
       }
     }
   )
 }
)


// function to check if a given account exists.

app.get('/apitechu/v2/accounts/:id',
 function (req,res){
   console.log("GET /apitechu/v2/accounts/:id");

   var id = req.params.id;
   var query = 'q={"iban" : "' + id + '"}';

   httpClient = requestJson.createClient(baseMLabURL);
   console.log("Cliente HTTP creado con query:" + query);

   httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error getting an account... :-("
         }
         res.status(500);
       }
       else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "account not found."
           };
           res.status(404);
         }
       }
       res.send(response);
      }
     );
   }
 )



//---------------------------------------------------------------------------------------------------------v1
//Function to login user
/*app.post('/apitechu/v2/login',
   function(req,res) {
       console.log("POST /apitechu/v2/login");
       console.log("Email " + req.body.email);
       console.log("Pwd " + req.body.password);

       //query to get the id of the user
       var query = 'q={"email" : "' + req.body.email + '" , "password" : "' + req.body.password + '"}';

       console.log (query);

       httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente HTTP creado");

       httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo usuario."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body[0];
               var id =  body[0].id;
               var queryPUT = 'q={"id" : ' + id + '}';
               console.log(queryPUT);
               var putBODY = '{"$set" : {"logged":true}}';

               httpClient.put("user?" + queryPUT + "&" + mLabAPIKey, JSON.parse(putBODY),
                function(errPUT, resMLabPUT, putBODY){
                    if (errPUT){
                       response = {
                         "msg" : "Error al hacer login"
                       };
                    } else{
                       console.log("logging in");
                       response = {
                         "msg" : "login correcto"
                       };
                    }
                  }
                );
             } else {
               response = {
                 "msg" : "Usuario no encontrado."
               };
               res.status(404);
             }
           }
          //  console.log("other");
           res.send(response);
         }
     );

   }
);

//implementation of the login method
app.put('/apitechu/v2/logout/:id',
   function(req,res) {
      console.log("POST /apitechu/v2/logout/:id");

      var query = 'q={"id" : "' + req.body.id + '"}';
      console.log (query);

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");



      //First search if the userId exists
      var putBODY = '{"$unset" : {"logged":true}}';

      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBODY),
      function(errPUT, resMLabPUT, bodyPUT){
          if (errPUT){
            mensaje = '{"msg" : "logout INcorrecto" }';
          } else{
             console.log("logging out");
             mensaje = '{"msg" : "logout correcto, yujuuu" }';
          }
          res.send(mensaje);
        }
      );
    }
);*/
//implementation of the add one user method
app.post('/apitechu/v1/users',
   function(req,res) {
       console.log("POST /apitechu/v1/users");

       //get the values in the header to create new user data
       /*var newUser = {
         "first_name" : req.headers.first_name,
         "last_name" : req.headers.last_name,
         "country" : req.headers.country
       };*/

       var newUser = {
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "country" : req.body.country
       };

       console.log("First name " + req.body.first_name);
       console.log("Last name " + req.body.last_name);
       console.log("Country " + req.body.country);

       var users = require('./usuarios.json');
       users.push(newUser);

       WriteUserDataToFile(users);

       res.send(users);
       console.log("New user BlBrothers")

       /*console.log("First name " + req.headers.first_name);
       console.log("Last name " + req.headers.last_name);
       console.log("Country " + req.headers.countusersry);*/
   }
);

//Function to delete user method
app.delete('/apitechu/v1/users/:id',
  function(req,res) {
    console.log("DELETE /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    WriteUserDataToFile (users);

    res.send(
      {
        "msg":"Deleted the just last"
      }
    );
  }
)

// Function to save data to a filesystem
// in: data is the data to be saved
function WriteUserDataToFile(data) {
  //add the component to manage filesystem
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    "./usuarios.json",
    jsonUserData,
    "utf8",
    function(err) {
      if (err) {
        console.log("err");
      } else {
        var msg = "Users file persisted!";
        console.log(msg);
      }
    }
  );
}

// Implementation of the post to see passing parameters
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res) {
    console.log("DELETE /apitechu/v1/monstruo/:p1/:p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Querystring");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)

//implementation of the read all users method
app.get('/apitechu/v1/users',
   function(req,res) {
       console.log("GET /apitechu/v1/users");
       res.sendFile('usuarios_emails.json', {root:__dirname});
       //res.sendFile('./usuarios.json'); //deprecated
   }
);
