var mocha = require ('mocha');
var chai = require ('chai');
var chaihttp = require('chai-http');

// if we want to test our API, to be unit test, we should start the server.
var server = require ('../server.js');

chai.use(chaihttp);

var should = chai.should();

//First test
describe ('Fisrt test',
function() {
  it ('Tests that google works', function(done){
    chai.request('https://www.google.com')
      .get('/')
      .end(
        function(err, res){
          console.log("Request has ended");
          //console.log(res);
          console.log(err);
          // debería tener una propiedad con el código 200, que es URL ok
          res.should.have.status(200);
          done();
        }
      );
    });
  }
);

// to test the APIapi
describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("This is the GET /apitechu/v1")
             done();
           }
         )
     }
   )
 }

),
it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('email');
             user.should.have.property('password');
           }
           done();
         }
       )
     }
   )
